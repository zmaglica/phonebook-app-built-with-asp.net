﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ZvonimirMaglicaHQCloudTestTask.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        // ActionResult prikazan ispod služi da bi angularJS mogao lodati HTML template u template folderu
        public ActionResult Contacts()
        {
            return PartialView();
        }

        public ActionResult Templates()
        {
            return PartialView();

        }

        //Contact Entites je automatski izgeneriran koristeći ADO.NET Entitiy Framework
        ContactsEntities db = new ContactsEntities();
        List<dbResultModel> globalResults;//Model za Listu da bi mogli mmodificirati i mijenjati listu
        //get
        public JsonResult GetAll()
        {

            var test = (from c in db.Contacts
                        from e in db.Emails.Where(x => x.id_contact == c.id).DefaultIfEmpty()
                        from p in db.Phones.Where(x => x.id_contact == c.id).DefaultIfEmpty()
                        from t in db.Tags.Where(x => x.id_contact == c.id).DefaultIfEmpty()
                        select new
                        {
                            id = c.id,
                            phones = (p != null ? p.number : null),
                            emails = (e != null ? e.email1 : null),
                            tags = (t != null ? t.tag1 : null),
                            firstname = c.firstname,
                            lastname = c.lastname,
                            address = c.address,
                            city = c.city,
                            bookmarked = c.bookmarked,
                            notes = c.notes
                        }).ToList();

            globalResults = (from contact in test
                             group contact by contact.id into grp
                             select new dbResultModel
                             {
                                 id = grp.Key,
                                 firstname = grp.First().firstname,
                                 lastname = grp.First().lastname,
                                 address = grp.First().address,
                                 city = grp.First().city,
                                 bookmarked = grp.First().bookmarked,
                                 notes = grp.First().notes,
                                 phones = grp.Where(x => x.phones != null).Select(x => x.phones).Distinct().ToArray(),
                                 emails = grp.Where(x => x.emails != null).Select(x => x.emails).Distinct().ToArray(),
                                 tags = grp.Where(x => x.tags != null).Select(x => x.tags).Distinct().ToArray()
                             }).ToList();
            Session["dbResult"] = globalResults;
            /*
             * pošto se je aplikacija za jednog user-a onda "možemo" spremiti u session varijablu sve rezultate iz baze podataka
             * session je dodan da bi napravili globalnu varijablu od liste u kojoj se nalazu svi podaci iz baze podataka.
             */
            return Json(globalResults, JsonRequestBehavior.AllowGet);
        }
        #region Add,Edit,Delete
        public string AddContact(Contact contact)
        {
            if (contact != null)
            {

                db.Contacts.Add(contact);
                db.SaveChanges();
                return "Contact Added";
            }
            else
            {
                return "Invalid Record";
            }
        }
        public string UpdateContact(Contact contact)
        {
            if (contact != null)
            {

                /*
                 * Kod Update kontakta prvo brišem editirani kontakt i onda ga dodajem. Razlog zašto ovo dodajem je u tome
                 * što kod kontakta gdje ima veliki broj tagova, mailova ili brojeva može doći da se korisnik predugo updatea jer
                 * prvo se provjeravaju koji se podaci updataju (a nekad može biti previše tih podataka) i onda se dodavaju novi podaci
                 * (tagovi, mobiteli i mailovi). Jednostavnije je da se izbrišu podaci i dodaju novi nego da se postojeći podaci prvo provjeravaju
                 * jesu li mijenjanji i tek onda se dodavaju novi ako ih ima. Po mom mišeljenu ovako je jednostavnije i vjerovatno optimiziranije
                 */
                int id = contact.id;
                var getContact = db.Contacts.Find(id);
                db.Contacts.Remove(getContact);
                db.SaveChanges();
                db.Contacts.Add(contact);
                db.SaveChanges();
                return "Contact Updated";

            }
            else
            {
                return "Invalid Record";
            }
        }

        public string DeleteContact(String contactId)
        {

            if (!String.IsNullOrEmpty(contactId))
            {
                try
                {
                    int id = Int32.Parse(contactId);
                    var getContact = db.Contacts.Find(id);

                    db.Contacts.Remove(getContact);
                    db.SaveChanges();
                    return "Contact Deleted";

                }
                catch
                {
                    return "Contact Not Found";
                }
            }
            else
            {
                return "Invalid Request";
            }
        }
        #endregion Add,Edit,Delete
        public JsonResult SearchContact(String query, int queryTask)
        {
            globalResults = (List<dbResultModel>)Session["dbResult"];
            /*
             * Umijesto da pretrazujemo bazu podataka jednostavnije je pretraziti Listu u kojoj se nalazu svi kontakti
             * Radi ovoga smo definirali session varijablu i u nju spremili sve kontakte
             * Da smo koristi tradicionalni načim trebalo bi pisati puno više koda, ovako smo u 5 linija riješili cijeli search
             */
            switch (queryTask)
            {
                case 0:
                    return Json(globalResults.Where(a => a.firstname.Equals(query, StringComparison.CurrentCultureIgnoreCase)), JsonRequestBehavior.AllowGet);
                case 1:
                    return Json(globalResults.Where(a => a.lastname.Equals(query, StringComparison.CurrentCultureIgnoreCase)), JsonRequestBehavior.AllowGet);
                case 2:
                    return Json(globalResults.Where(a => a.tags.Contains(query, StringComparer.OrdinalIgnoreCase)), JsonRequestBehavior.AllowGet);
                default:
                    break;
            }
            return null;

        }

    }
}