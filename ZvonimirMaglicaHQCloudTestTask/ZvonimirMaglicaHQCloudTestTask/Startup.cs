﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ZvonimirMaglicaHQCloudTestTask.Startup))]
namespace ZvonimirMaglicaHQCloudTestTask
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
