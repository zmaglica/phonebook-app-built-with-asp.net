﻿using System.Web;
using System.Web.Optimization;

namespace ZvonimirMaglicaHQCloudTestTask
{

    public class BundleConfig
    {

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/tools").Include(
            "~/Scripts/angular.js",
            "~/Scripts/angular-route.js"));

            bundles.Add(new ScriptBundle("~/bundles/application")
            .IncludeDirectory("~/Scripts/application/", "*.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
            "~/Content/bootstrap.css",
            "~/Content/custom.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
